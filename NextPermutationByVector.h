#pragma once

#include<vector>
#include"NP.h"

class NextPermutationByVector: public NP {
public:
	NextPermutationByVector() {};
	~NextPermutationByVector() {};

	int getSum(int left, int right);
	void insert(int pos, int value);
	void addToSegment(int left, int right, int value);
	void change(int pos, int newValue);
	void nextPermutation(int left, int right);
private:
	std::vector<int> arr_;
};