#pragma once

class NP {
public:
	virtual int getSum(int, int) { return 0; };
	virtual void insert(int pos, int value) {};
	virtual void addToSegment(int left, int right, int value) {};
	virtual void change(int pos, int newValue) {};
	virtual void nextPermutation(int left, int right) {};
};