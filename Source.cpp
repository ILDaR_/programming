#include<iostream>
#include"NextPermutation.h"
#include"NextPermutationByVector.h"
#include"TestSystem.h"

int main()
{
	NextPermutation a1, a2, a3;
	NextPermutationByVector b1, b2, b3;

	TestSystem test;
	test.test1(a1, b1);
	test.test2(a2, b2);
	test.test3(a3, b3);
	system("pause");
	return 0;
}