#pragma once
#include"NP.h"

class NextPermutation: public NP{
public:
	NextPermutation() :root(nullptr) {};
	~NextPermutation();

	int getSum(int left, int right);
	void insert(int pos, int value);
	void addToSegment(int left, int right, int value);
	void change(int pos, int newValue);
	void nextPermutation(int left, int right);

	struct Node {
		Node* left;
		Node* right;
		Node* parent;
		int treeSize;
		int sum;
		int addValue; //�������� �� ������� ����� ���������
		bool needReverse;
		int value;

		int leftElement, rightElement; //������� ����� � ������ ����
		int lengthInc, lengthDec; //����� �������.\����. �������. �� ��������\��������

		Node(int value_) : 
			left(0), right(0), parent(0), sum(value_), treeSize(1), 
			needReverse(false), value(value_), addValue(0),
			leftElement(value_), rightElement(value_), lengthInc(1), lengthDec(1) {};
		Node():
			left(0), right(0), parent(0), sum(0), treeSize(1), 
			needReverse(false), value(0), addValue(0),
			leftElement(0), rightElement(0), lengthInc(1), lengthDec(1) {};
	};
private:
	Node* root;

	bool isLeft(Node* x);
	bool isRight(Node* x);
	void leftRotate(Node* x);
	void rightRotate(Node* x);
	void splay(Node* x);
	void updateAll(Node* x); //�������� ����� � ���������� ������ � ���������
	void updateNextPermutation(Node* x);
	void pushReverse(Node* x);
	void pushAddValue(Node* x);
	Node* merge(Node* &tree1, Node* &tree2);
	void split(int pos, Node* &tree1, Node* &tree2);
	Node* find(int pos, Node* x);
	Node* findInReversedSearchTree(int value, Node* head);
};