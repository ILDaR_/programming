#pragma once

#include<iostream>
#include<random>
#include<time.h>
#include<math.h>
#include"NP.h"
#include"Fuctions.h"


class TestSystem {
public:
	TestSystem();

	void test1(NP &Obj1, NP &Obj2);
	void test2(NP &Obj1, NP &Obj2);
	void test3(NP &Obj1, NP &Obj2);
private:
	int getRand(int d);
};

TestSystem::TestSystem() {
	setlocale(0, "");
	srand(time(NULL));
}

void TestSystem::test1(NP &Obj1, NP &Obj2)
{
	enum {
		addToSegment, insert, getSum, change, nextPermutation
	};
	int numOperations, d;
	std::cout << "Test #1. �������� ���� ��������.\n";
	std::cout << "������� ����� ��������: ";
	std::cin >> numOperations;
	std::cout << "������� �������� ��������: ";
	std::cin >> d;

	bool isIdentic = true;
	int size = 0;
	int typeOp;

	for (int i = 0; i < numOperations; i++) {
		
		typeOp = getRand(5);
		if (size == 0) {
			typeOp = 1;
		}
		
		switch(typeOp)		{
		case addToSegment:{
				int right = getRand(size);
				int left = getRand(right + 1);
				int val = getRand(d);
				Obj1.addToSegment(left, right, val);
				Obj2.addToSegment(left, right, val);
				break;
			}
		case insert: {
				int pos;
				if (size == 0) {
					pos = 0;
				}
				else {
					pos = getRand(size);
				}
				int val = getRand(d);
				Obj1.insert(pos, val);
				Obj2.insert(pos, val);
				size++;
				break;
			}	
		case getSum: {
				int right = getRand(size);
				int left = getRand(right + 1);
				if (Obj1.getSum(left, right) != Obj2.getSum(left, right)) {
					std::cout << "ERROR\n";
					isIdentic = false;
				}
				else {
					std::cout << "Correct Sum " << Obj2.getSum(left, right) <<"\n";
				}
				break;
			}
		case change:	{
				int pos = getRand(size);
				int val = getRand(d);
				Obj1.change(pos, val);
				Obj2.change(pos, val);
				break;
			}
		case nextPermutation:{
				int right = getRand(size);
				int left = getRand(right + 1);
				Obj1.nextPermutation(left, right);
				Obj2.nextPermutation(left, right);
				break;
			}
		}
	}

	if (isIdentic) {
		std::cout << "���������\n\n";
	}
	else {
		std::cout << "������\n\n";
	}
}

void TestSystem::test2(NP &Obj1, NP &Obj2)
{
	std::cout << "Test #2. N! ������������.\n";
	std::cout << "������� ������ �������: ";
	int length;
	std::cin >> length;

	for (int i = 0; i < length; i++) {
		Obj1.insert(i, i);
		Obj2.insert(i, i);
	}

	for (int i = 0; i < fact(length); i++) {
		Obj1.nextPermutation(0, length - 1);
		Obj2.nextPermutation(0, length - 1);
	}

	bool isIdentic = true;
	for (int i = 0; i < length; i++) {
		if (Obj1.getSum(i, i) != Obj2.getSum(i, i)) {
			isIdentic = false;
		}
	}

	if (isIdentic) {
		std::cout << "���������\n\n";
	}
	else {
		std::cout << "������\n\n";
	}
}

void TestSystem::test3(NP &Obj1, NP &Obj2)
{
	std::cout << "Test #3. �������� �������� ����.\n";
	std::cout << "������� ���������� ������: ";
	int numOperations;
	std::cin >> numOperations;
	std::cout << "������� �������� ��������: ";
	int d;
	std::cin >> d;
	for (int i = 0; i < d; i++) {
		Obj1.insert(i, i);
		Obj2.insert(i, i);
	}

	bool isIdentic = true;
	for (int i = 0; i < numOperations; i++) {
		int right = getRand(d);
		int left = getRand(right + 1);
		if (Obj1.getSum(left, right) != Obj2.getSum(left, right)) {
			isIdentic = false;
		}
	}

	if (isIdentic) {
		std::cout << "���������\n\n";
	}
	else {
		std::cout << "������\n\n";
	}
}

int TestSystem::getRand(int d)
{
	int i = int(std::abs(rand())) % d;
	return i;
}