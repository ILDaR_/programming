#include<numeric>
#include<algorithm>
#include"NextPermutationByVector.h"



void NextPermutationByVector::change(int pos, int value)
{
	arr_[pos] = value;
}

void NextPermutationByVector::addToSegment(int left, int right, int value)
{
	for (int i = left; i <= right; i++) {
		arr_[i] += value;
	}
}

int NextPermutationByVector::getSum(int left, int right)
{
	return std::accumulate(arr_.begin() + left, arr_.begin() + right + 1, 0);
}

void NextPermutationByVector::insert(int pos, int value)
{
	arr_.insert((arr_.begin() + pos), value);
}

void NextPermutationByVector::nextPermutation(int left, int right)
{
	std::next_permutation(arr_.begin() + left, arr_.begin() + right + 1);
}