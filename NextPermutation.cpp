#include<assert.h>
#include<iostream>
#include<queue>
#include"NextPermutation.h"

NextPermutation::~NextPermutation()
{
	if (root == nullptr) {
		return;
	}
	std::queue<Node*> q;
	q.push(root);
	
	while (!q.empty()) {
		Node* H = q.front();
		if (H->left) {
			q.push(H->left);
		}
		if (H->right) {
			q.push(H->right);
		}
		q.pop();
		delete H;
	}
	
}

bool NextPermutation::isLeft(Node* x)
{
	if (x->parent == nullptr) {
		return false;
	}
	return (x->parent->left == x);
}

bool NextPermutation::isRight(Node* x)
{
	if (x->parent == nullptr) {
		return false;
	}
	return (x->parent->right == x);
}

void NextPermutation::pushReverse(Node* x) //������������ reverse � �����
{
	if (x == nullptr) return;
	if (x->needReverse) {
		std::swap(x->leftElement, x->rightElement);
		std::swap(x->lengthDec, x->lengthInc);
		std::swap(x->left, x->right);
		x->needReverse = false;
		if (x->left) {
			if (x->left->needReverse) {
				x->left->needReverse = false;
			}
			else {
				x->left->needReverse = true;
			}
		}
		if (x->right) {
			if (x->right->needReverse) {
				x->right->needReverse = false;
			}
			else {
				x->right->needReverse = true;
			}
		}
	}
}

void NextPermutation::pushAddValue(Node* x)
{
	if (x == nullptr) return;
	if (x->addValue != 0) {
		x->leftElement += x->addValue;
		x->rightElement += x->addValue;
		x->sum += x->addValue * x->treeSize;
		if (x->left) {
			x->left->addValue += x->addValue;
		}
		if (x->right) {
			x->right->addValue += x->addValue;
		}
		x->value += x->addValue;
		x->addValue = 0;
	}
}

void NextPermutation::updateNextPermutation(Node* x)//�������� ��������, �������� � 
							//left/right element � ��������������� ���������� reverse
{
	if (x == nullptr) return;

	pushReverse(x);
	pushReverse(x->left);
	pushReverse(x->right);
	
	if (x->left) {
		x->leftElement = x->left->leftElement + x->left->addValue;
	}
	else {
		x->leftElement = x->value;
	}
	
	if (x->right) {
		x->rightElement = x->right->rightElement + x->right->addValue;
	}
	else {
		x->rightElement = x->value;
	}

	
	if (x->left && x->right) {
		if (x->right->lengthDec == x->right->treeSize) {
			if (x->value > x->right->leftElement) {
				if (x->value < x->left->rightElement) {
					x->lengthDec = x->left->lengthDec + x->right->lengthDec + 1;
				}
				else {
					x->lengthDec = x->right->lengthDec + 1;
				}
			}
			else {
				x->lengthDec = x->right->lengthDec;
			}
		}
		else {
			x->lengthDec = x->right->lengthDec;
		}

		if (x->left->lengthInc == x->left->treeSize) {
			if (x->value > x->left->rightElement) {
				if (x->value < x->right->leftElement) {
					x->lengthInc = x->left->lengthInc + x->right->lengthInc + 1;
				}
				else {
					x->lengthInc = x->left->lengthInc + 1;
				}
			}
			else {
				x->lengthInc = x->left->lengthInc;
			}
		}
		else {
			x->lengthInc = x->left->lengthInc;
		}
	}
	else if (x->left) {
		if (x->value < x->left->rightElement) {
			x->lengthDec = x->left->lengthDec + 1;
		}
		else {
			x->lengthDec = 1;
		}
		
		if (x->left->lengthInc == x->left->treeSize) {
			if (x->value > x->left->rightElement) {
				x->lengthInc = x->left->lengthInc + 1;
			}
			else {
				x->lengthInc = x->left->lengthInc;
			}
		}
		else {
			x->lengthInc = x->left->lengthInc;
		}
	}
	else if (x->right) {
		if (x->value < x->right->leftElement) {
			x->lengthInc = x->right->lengthInc + 1;
		}
		else {
			x->lengthInc = 1;
		}

		if (x->right->lengthDec == x->right->treeSize) {
			if (x->value > x->right->leftElement) {
				x->lengthDec = x->right->lengthDec + 1;
			}
			else {
				x->lengthDec = x->right->lengthDec;
			}
		}
		else {
			x->lengthDec = x->right->lengthDec;
		}
	}
	else {
		x->lengthDec = x->lengthInc = 1;
		x->leftElement = x->rightElement = x->value + x->addValue;
	}

}

void NextPermutation::updateAll(Node* x) // ������������� ��� ����
{
	if (x == nullptr) return;
	pushAddValue(x);
	if (x->left && x->right) {
		x->sum = x->left->sum + x->left->addValue * x->left->treeSize + x->right->sum
			+ x->right->addValue * x->right->treeSize + x->value;
		x->treeSize = x->left->treeSize + x->right->treeSize + 1;
	}
	else if (x->left) {
		x->sum = x->left->sum + x->left->addValue * x->left->treeSize + x->value;
		x->treeSize = x->left->treeSize + 1;
	}
	else if (x->right) {
		x->sum = x->right->sum + x->right->treeSize * x->right->addValue + x->value;
		x->treeSize = x->right->treeSize + 1;
	}
	else {
		x->sum = x->value;
		x->treeSize = 1;
	}
	updateNextPermutation(x);
}

void NextPermutation::leftRotate(Node* x)
{
	Node* p = x->parent;

	p->right = x->left;
	if (x->left) {
		x->left->parent = p;
	}

	x->parent = p->parent;

	if (!p->parent) {
		root = x;
	}
	else if (isLeft(p)) {
		p->parent->left = x;
	}
	else {
		p->parent->right = x;
	}

	x->left = p;
	p->parent = x;

	updateAll(p);
	updateAll(x);
}

void NextPermutation::rightRotate(Node* x)
{
	Node* p = x->parent;

	p->left = x->right;
	if (x->right) {
		x->right->parent = p;
	}

	x->parent = p->parent;

	if (!p->parent) {
		root = x;
	} else if (isLeft(p)) {
		p->parent->left = x;
	} else {
		p->parent->right = x;
	}

	x->right = p;
	p->parent = x;

	updateAll(p);
	updateAll(x);
}

void NextPermutation::splay(Node* x)
{
	while (x->parent) {
		if (x->parent->parent == nullptr) { //zig
			if (isLeft(x)) rightRotate(x);
			else leftRotate(x);
		}
		else if (isLeft(x) && isLeft(x->parent)) { //zig-zig
			rightRotate(x->parent);
			rightRotate(x);
		}
		else if (isRight(x) && isRight(x->parent)) { //zig-zig
			leftRotate(x->parent);
			leftRotate(x);
		}
		else if (isLeft(x) && isRight(x->parent)) { //zig-zag
			rightRotate(x);
			leftRotate(x);
		}
		else { //zig-zag
			leftRotate(x);
			rightRotate(x);
		}
	}
}

NextPermutation::Node* NextPermutation::find(int pos, Node* x)
{
	assert(pos <= x->treeSize);
	while (true) {
		pushReverse(x);
		pushAddValue(x);

		if (x->left && x->left->treeSize == pos) {
			return x;
		}
		if (x->left == nullptr) {
			if (pos == 0) {
				return x;
			}
			x = x->right;
			pos--;
			continue;
		}
		if (pos < x->left->treeSize) {
			x = x->left;
			continue;
		}
		else {
			pos = pos - x->left->treeSize - 1;
			x = x->right;
			continue;
		}
	}
}

NextPermutation::Node* NextPermutation::merge(Node* &tree1, Node* &tree2)
{
	if (tree1 == nullptr) {
		return tree2;
	}
	if (tree2 == nullptr) {
		return tree1;
	}
	Node* maxNode = find(tree1->treeSize - 1, tree1);
	splay(maxNode);
	maxNode->right = tree2;
	tree2->parent = maxNode;
	updateAll(maxNode);
	return maxNode;
}

void NextPermutation::split(int pos, Node* &tree1, Node* &tree2)
{
	assert(pos <= root->treeSize);
	if (pos == root->treeSize) {
		Node* curH = find(pos - 1, root);
		splay(curH);
		tree1 = curH;
		tree2 = nullptr;
		root = nullptr;
		return;
	}
	Node* curH = find(pos, root);
	splay(curH);
	tree1 = curH->left;
	tree2 = curH;
	if (curH->left) {
		curH->left->parent = nullptr;
	}
	curH->left = nullptr;
	root = nullptr;
	updateAll(tree2);
}

void NextPermutation::insert(int pos, int value)
{
	if (root == nullptr) {
		assert(pos == 0);
		Node* newH = new Node(value);
		root = newH;
		return;
	}

	assert(pos <= root->treeSize);
	if (pos == root->treeSize) {
		Node* newH = new Node(value);
		root = merge(root, newH);
		splay(newH);
	}
	else {
		Node* tree1 = nullptr;
		Node* tree2 = nullptr;
		split(pos, tree1, tree2);
		Node* newH = new Node(value);
		Node* h = merge(tree1, newH);
		root = merge(h, tree2);
	}
}

void NextPermutation::change(int pos, int value)
{
	assert(pos < root->treeSize);
	Node* curH = find(pos, root);
	curH->sum += - curH->value + value;
	curH->value = value;
	splay(curH);
}

int NextPermutation::getSum(int left, int right)
{
	Node* tree1 = nullptr;
	Node* tree2 = nullptr;
	Node* tree3 = nullptr;
	assert(right + 1 <= root->treeSize);
	split(right + 1, tree1, tree3);
	root = tree1;
	split(left, tree1, tree2);
	int s = tree2->sum + tree2->addValue * tree2->treeSize;

	Node* h = merge(tree1, tree2);
	root = merge(h, tree3);
	return s;
}

void NextPermutation::addToSegment(int left, int right, int value)
{
	Node* tree1 = nullptr;
	Node* tree2 = nullptr;
	Node* tree3 = nullptr;
	assert(left + 1 <= root->treeSize);
	split(right + 1, tree1, tree3);
	root = tree1;
	split(left, tree1, tree2);
	tree2->addValue += value;
	tree1 = merge(tree1, tree2);
	root = merge(tree1, tree3);
	return;
}

NextPermutation::Node*  NextPermutation::findInReversedSearchTree(int value, Node* head)
{
	if (head->value <= value) {
		if (head->left) {
			return findInReversedSearchTree(value, head->left);
		}
	}
	else {
		if (head->right) {
			if (value > head->right->leftElement) {
				return head;
			}
			else {
				return findInReversedSearchTree(value, head->right);
			}
		}
		else {
			return head;
		}
	}
}

void NextPermutation::nextPermutation(int left, int right)
{
	if (left == right) return;
	Node* t1 = root;
	Node* t2 = nullptr;
	Node* t3 = nullptr;
	Node* t4 = nullptr;
	assert(right + 1 <= root->treeSize);
	split(right + 1, t1, t4);
	root = t1;
	split(left, t1, t2);
	root = t2;
	if (t2->lengthDec == t2->treeSize) {
		t2->needReverse = true;
		updateAll(t2);
		t1 = merge(t1, t2);
		root = merge(t1, t4);
	}
	else {
		root = t2;
		split(t2->treeSize - t2->lengthDec, t2, t3);
		Node* y = find(t2->treeSize - 1, t2);
		splay(y);
		Node* z = findInReversedSearchTree(y->value, t3);
		splay(z);
		std::swap(y->value, z->value);
		updateAll(y);
		updateAll(z);
		z->needReverse = true;
		updateAll(z);
		t1 = merge(t1, y);
		z = merge(z, t4);
		root = merge(t1, z);
	}
}